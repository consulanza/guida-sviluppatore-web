# Strumenti per lo sviluppo del software

## Php IDE

Le applicazioni IDE offrono ambienti integrati per lo sviluppo del sofware, normalmente permettono di gestire il ciclo
di sviluppo fornendo strumenti per editing, controllo del codice, debug, gestione DB ecc.  

### PhpStorm

PhpStorm è un IDE che disponde di parecchi plugin per lo sviluppo di applicazioni web. L'integrazione con GIT, composer,
npm. yarn, Vue, Angular e altri software standard ne fanno uno strumento versatile

#### Server integrato

![img.png](images/phpstorm_serve_01.png)


#### Debug integrato

![img.png](images/phpstorm_debug_01.png)

#### Integrazione con GIT

![phpStorm - Integrazione con GIT](images/phpstorm_git_01.png)

#### Integrazione con il database

![PhpStorm - integrazione con il databae](images/phpstorm_database_01.png)

### VSCode

VSCode è un ambiente integrato (IDE) per lo sviluppo delle applicazioni web

## GIT

Git è un sistema di versioning moderno per la gestione e l'accesso ai repository dei sorgenti

## XDebug

Xdebug è il debugger per PHP e permette l'esecuzione ed il controllo passo-passo delle applicazioni

## REST API Client

Un client REST API ci permette di provare le chiamate senza dover ricorrere all'interfaccia utente ufficiale.

### Advancer Rest Client

[Advanced REST Client](https://install.advancedrestclient.com/) è un client API idoneo a provare le chiamate API REST delle applicazioni

Un client REST API ci permette di provare le chiamate senza dover ricorrere all'interfaccia utente ufficiale. Tra i più
diffusi c'è [Postman](https://www.postman.com/), io utilizzo 

![Advanced REST Client](images/advanced_rest_client_01.png)

### Postman

[Postman](https://www.postman.com/) è un client API idoneo a provare le chiamate API REST delle applicazioni


## Database Workbench

Si trovano diversi tool per manipolare i database, spesso in ambiente di sviluppo viene utilizzato phpMyAdmin; questa
soluzione non è molto efficiente per il lavoro su sulla singola postazione in quanto richiede la presenza di un server
Apache che lo contenga. Tale server potrebbe essere implementato in un container apposito - quindi indipendente dal db -
o nello stesso container del servizio database. Vista la semplicità nell'utilizzo di qualsiasi altro client, possiamo
utilizzare strumento come lo storivo [MySQL Workbench](https://www.mysql.com/products/workbench/) altri più generici
che possono lavorare con tutti i più diffusi database, come [DBEaver](https://dbeaver.io/)

![DB Eaver](images/dbeaver_01.png)

## Docker

[Docker](Docker.md) è un sistema di contenitori (container) che offrono uno o più servizi, normalmente correlati, senza
dipendenze dal sistema ospite. Un container è simile ad una macchina virtuale che contiene tutto il necessario - e non
altro - per poter offrire i propri servizi che corrispondono a server come Apache, NginX, Database server ecc.

L'utilizzo dei container sulle macchine di sviluppo permette una gestione pù semplice in quanto non andranno installati
i diversi servizi (software) necessari ma saranno offerti dai container. Un ulteriore vantaggio è dato dalla possibilità
di mantenere container con diverse versioni dello stesso servizio (come MySQL e MariaDB), permettendo ad esempio di
passare da MySQL a MariaDB o da una versione di MySQL ad un'altra - semplicemente spegnendo un container ed avviandone
un altro - per testare il proprio codice in situazioni diverse.  
