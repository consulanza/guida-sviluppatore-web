# Postazione di sviluppo Web

## Setup di una postazione Linux

Quanto segue è un esempio di postazione Linux per lo sviluppo di aplicazioni Web

### Servizi

L'ambiente di sviluppo deve avere a disposizione i servizi che saranno utilizzati in produzione. Nonostante sia
possibile installare tutti i server necessari in modalità nativa, per facilità possiamo utilizzare dei container
[Docker](Docker.md) che richiederanno minori competenze per essere installati e rispecchiano meglio un ambiente
distribuito dove i diversi servizi siano erogati da nodi diversi.

### PHP

Per lo sviluppo di applicazioni Web abbiamo scelto di utilizzare PHP, che è necessario per il lato backend.

#### PHP Cli



```bash
$ php artisan serve
```

questo comando avvia un server web integrato che utilizza la versione CLI (linea di comando) di PHP e simula 
l'esecuzione come se l'applicazione girasse su un vero web server.

Questo significa che non abbiamo la necessità di installare e gestire componenti come Apache (web server) o PHP-FPM,
che richiedono competenze che esulano dal normale lavore dello sviluppatore ed aumentano la complessità del tutto senza
aggiungere particolari vantaggi.

Sui sistemi Linux è possibile installare e far convivere diverse versioni di PHP; purtroppo non è una soluzione
**_out-of-the-box_** e richiede alcuni aggiustamenti ma è utile se si ha la necessità di lavorare su applicazioni
sviluppate con versioni PHP differenti o che vanno aggiornate alla versione corrente di PHP.

Per installare versioni differenti di PHP dovremo seguire diversi passi:

##### Dipendenze

```bash
$ sudo apt-get install software-properties-common ca-certificates lsb-release apt-transport-https
```

##### Impostazione repository alternativo per PHP

```bash
$ LC_ALL=C.UTF-8 sudo add-apt-repository ppa:ondrej/php 
```

nel repository pndrej/php sono mantenute tutte le versioni PHP per Ubuntu.

##### Aggiorniamo il sistema dei pacchetti per acquisire i nuovi indici

```bash
$ sudo apt-get update 
```

##### Installiamo le versioni necessarie di PHP

```bash
$ sudo apt-get install php7.4
```

```bash
$ sudo apt-get install php8.2
```

##### Estensioni PHP

Saranno necessarie anche parecchie estensioni, che andranno installate per ogni versione di PHP con il comando:

```bash
sudo apt-get install php<versione>-<estensione>
```

ad esempio:

```bash
apt-get install php8.2-gzip
```

#### Selezione versione PHP da utilizzare

Per poter lavorare con le differenti versioni, dovremo preparare uno script che configuri la versione di default da
utilizzare, ad esempio **set-php-version.sh** conebebte:

```bash
#!/bin/bash
#
SELECTED_VERSION=$1
#
sudo update-alternatives --set php /usr/bin/php${SELECTED_VERSION}
sudo update-alternatives --set phar /usr/bin/phar${SELECTED_VERSION}
sudo update-alternatives --set phar.phar /usr/bin/phar.phar${SELECTED_VERSION}
```

potremo quindi impostare al volo la versione desiderata di PHP con il comando:

```bash
$ set-php-bersion.sh 7.4
```

```bash
$ set-php-bersion.sh 8.2
```


##### Estensioni più utilizzate

Le estensioni comunemente più usate comprendono:

- **common**
- **cli**
- amqp
- bcmath
- bz2
- calendar
- ctype
- curl
- dom
- exif
- ffi
- fileinfo
- ftp
- gd
- gettext
- gmagick
- iconv
- imagick
- imap
- intl
- json
- mbstring
- mcrypt
- mysqli
- opcache
- pdo
- **phar**
- posix
- readline
- redis
- shmop
- simplexml
- sockets
- sysvmg
- sysvhm
- tokenizer
- **xdebug**
- xmlreader
- xsl
- yaml
- zip

#### XDebug

XDebug consente il debugging del cosice PHP, generalmente tramite IDE come PhpStorm o VSCode

Per utilizzarlo deve essere installato il modulo per le versioni interessate, ad esempio per 8.2:

```bash
$ sudo apt-get install php8.1-xdebug
```
##### Pagina Web

Per poter debuggare i sorgenti dell'applicazione web sarà necessario aggiungere nel relativo file .ini le opzioni:

```ini
xdebug.mode=debug
xdebug.discover_client_host=true
```

inoltre la porta utilizzata di default è 9003 quindi bisogna assicurarsi che non venga utilizzata da altri servizi come
Elasticsearch.

##### Script PHP
Per debuggare uno script PHP da linea comando basta aggiungere le opzioni:

- dxdebug.mode=debug
- dxdebug.client_host=127.0.0.1
- dxdebug.client_port=9003
- dxdebug.start_with_request=yes

quindi, ad esempio:

```bash
php -dmemory_limit=-1 -dxdebug.mode=debug -dxdebug.client_host=127.0.0.1 -dxdebug.client_port=9003 -dxdebug.start_with_request=yes
```

nota: 
l'opzione ```-dmemory_limit=-1``` consente allo script di non essere interrotto quando supera i limiti di memoria
impostati nel php.ini. L'utilizzo con script che vadano in loop incontrollato potrebbe portare ad un crash del sistema a
causa dell'esaurimento della memoria.

#### composer

### Tool di sviluppo



#### REST API Client

Un client REST API ci permette di provare le chiamate senza dover ricorrere all'interfaccia utente ufficiale. Tra i più 
diffusi c'è [Postman](https://www.postman.com/), io utilizzo [Advanced REST Client](https://install.advancedrestclient.com/)

![Advanced REST Client](images/advanced_rest_client_01.png)

