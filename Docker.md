# Docker

Docker è un sistema di contenitori (container) che offrono uno o più servizi, normalmente correlati, senza
dipendenze dal sistema ospite. Un container è simile ad una macchina virtuale che contiene tutto il necessario - e non
altro - per poter offrire i propri servizi che corrispondono a server come Apache, NginX, Database server ecc.

L'utilizzo dei container sulle macchine di sviluppo permette una gestione pù semplice in quanto non andranno installati
i diversi servizi (software) necessari ma saranno offerti dai container. Un ulteriore vantaggio è dato dalla possibilità
di mantenere container con diverse versioni dello stesso servizio (come MySQL e MariaDB), permettendo ad esempio di
passare da MySQL a MariaDB o da una versione di MySQL ad un'altra - semplicemente spegnendo un container ed avviandone
un altro - per testare il proprio codice in situazioni diverse.  

Di seguito alcuni esempi per la creazione di container semplici per i servizi necessari

## Database

Utilizziamo docker-compose per creare un container con il database server. Il servizio verrà erogato in modo quasi
trasparente rispetto ad installare direttamente il software sul sistema, ad eccezione della necessità di specificare
l'indirizzo host 127.0.0.1 per la connessione. Per il resto potremo utilizzare qualsiasi client senza impedimenti.

Questo tipo di impostazione riflette anche meglio una situazione di produzione generica in cui diversi servizi siano erogati da
macchine differenti.

Potremo quindi utilizzare i il client mysdql per restorare il db con il seguente comando:

```bash
$ mysql -h127.0.0.1 -u<user> -p <database> < db_dump.sql
```

o utilizzare client grafici come [DBEaver](https://dbeaver.io/) che normalmente sono più comodi delle interfacce web
come phpMyAdmin e ci evitano la necessità di installarlo, con relativo webserver, nel container.

### MySQL 8

Creiamo una directory per il nostro container

```bash
$ mkdir -p ~/Workspace/Docker/MySQL8
```

creiamo il file ~/Workspace/Docker/MySQL8/docker-compose.yaml

```yaml
version: '3.8'
services:
  db:
    image: mysql:8.2
    cap_add:
      - SYS_NICE
    restart: always
    environment:
      - MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD}
    ports:
      - '3306:3306'
    volumes:
      - ./volumes/var/lib/mysql:/var/lib/mysql
      - ./volumes/var/log:/var/log
      - ./volumes/etc/mysql:/var/etc/mysql
volumes:
  volumes:
    driver: local
```

creiamo il file ~/Workspace/Docker/MySQL8/.env

```ini
MYSQL_ROOT_PASSWORD=<password per utente db root>
```

installiamo ed avviamo il container con il comando
```bash
$ docker-compose up -d
```

che provvederà, se necessario, a scaricare l'immagine per il container perima di avviarlo.


### Redis

Redis (cache server) è facilmente compreso nei requisiti di applicazione Web, ad esempio è richiesto da Magento 2 e può essere
utilizzato dalle applicazione Laravel.

Creiamo una directory per il nostro container

```bash
$ mkdir -p ~/Workspace/Docker/MySQL8
```

creiamo il file ~/Workspace/Docker/Redis/docker-compose.yaml

```yaml
version: '3.8'
services:
  cache:
    image: redis:6.2-alpine
    restart: always
    ports:
      - '6379:6379'
    command: redis-server --save 20 1 --loglevel warning
    volumes:
      - cache:/data
volumes:
  cache:
    driver: local
```

installiamo ed avviamo il container con il comando
```bash
$ docker-compose up -d
```

che provvederà, se necessario, a scaricare l'immagine per il container perima di avviarlo.

### Soketi

Socketi è un WebSockets server simile a Pusher. Può essere utilizzato per implementare la gestione di eventi tra
il frontend ed il backend.

Creiamo una directory per il nostro container

```bash
$ mkdir -p ~/Workspace/Docker/Soketi
```

creiamo il file ~/Workspace/Docker/Soketi/docker-compose.yaml

```yaml
version: '3.9'
services:
  # ...

  soketi:
    container_name: 'soketi_server'
    restart: unless-stopped
    image: 'quay.io/soketi/soketi:0.17-16-alpine'
    ports:
      - '${SOKETI_PORT:-6001}:6001'
      - '${SOKETI_METRICS_SERVER_PORT:-9601}:9601'
    environment:
      - SOKETI_DEBUG='${DEBUG:-1}'
      - DEFAULT_APP_ID='${PUSHER_APP_ID:-some-id}'
      - DEFAULT_APP_KEY='${PUSHER_APP_KEY:-app-key}'
      - DEFAULT_APP_SECRET='${PUSHER_APP_SECRET:-some-app-secret}'
      - PUSHER_HOST='${PUSHER_HOST:-127.0.0.1}'
      - PUSHER_PORT= '${PUSHER_PORT:-6001}'
      - PUSHER_SCHEME='${PUSHER_SCHEME:-http}' # or https
      - METRICS_SERVER_PORT=${METRICS_SERVER_PORT:-9601}
      - DEFAULT_APP_ENABLE_CLIENT_MESSAGES=${DEFAULT_APP_ENABLE_CLIENT_MESSAGES:-false}
    networks:
      - soketi_network

networks:
  soketi_network:
    driver: bridge
```

creiamo il file ~/Workspace/Docker/Soketi/.env

```ini
SOKETI_PORT=6001
SOKETI_METRICS_SERVER_PORT=9601
DEBUG=1
PUSHER_APP_ID=app-id
PUSHER_APP_KEY=app-key
PUSHER_APP_SECRET=app-secret
PUSHER_HOST=127.0.0.1
PUSHER_PORT=6001
PUSHER_SCHEME=http
DEFAULT_APP_ENABLE_CLIENT_MESSAGES=false # make true if you want to enable client events
```

installiamo ed avviamo il container con il comando
```bash
$ docker-compose up -d
```

che provvederà, se necessario, a scaricare l'immagine per il container perima di avviarlo.


### Elasticsearch

Elasticsearch è un motore di ricerca molto diffuso e può essere utilizzato dalla applicazioni Web

Creiamo una directory per il nostro container

```bash
$ mkdir -p ~/Workspace/Docker/MySQL8
```

creiamo il file ~/Workspace/Docker/Elasticsearch/docker-compose.yaml

```yaml

version: '3'
services:
  elasticsearch:
    image: elasticsearch:8.8.0
    ports:
      # NON UTILIZZARE LA PORTA 9003 perché usata da XDebug
      - 9200:9200
    environment:
      - discovery.type=single-node
      - xpack.security.enabled=false

```

**nota**: Elasticsearch potrebbe utilizzare anche la porta 9003; per le postazioni di sviluppo che debbano debuggare il
codice PHP, è consigliabile non assegnarla a Elasticsearch in quanto necessaria a XDebug.

installiamo ed avviamo il container con il comando
```bash
$ docker-compose up -d
```

che provvederà, se necessario, a scaricare l'immagine per il container perima di avviarlo.


#### Esempio servizi attivati

Possiamo visualizzare l'elenco dei servizi che abbiamo attivato con il comando:

```bash
$ sudo docker ps
```

```bash
CONTAINER ID   IMAGE                                  COMMAND                  CREATED       STATUS       PORTS                                                                                  NAMES
4de63a268acb   quay.io/soketi/soketi:0.17-16-alpine   "node /app/bin/serve…"   2 hours ago   Up 2 hours   0.0.0.0:6001->6001/tcp, :::6001->6001/tcp, 0.0.0.0:9601->9601/tcp, :::9601->9601/tcp   soketi_server
1c814b1a0213   redis:6.2-alpine                       "docker-entrypoint.s…"   2 days ago    Up 2 days    0.0.0.0:6379->6379/tcp, :::6379->6379/tcp                                              redis_cache_1
7d84c0dc310d   mysql:8.2                              "docker-entrypoint.s…"   4 days ago    Up 3 days    0.0.0.0:3306->3306/tcp, :::3306->3306/tcp, 33060/tcp                                   developer_db_1
```

tutti i servizi elencati sono accessibili dalla postazione nello stesso modo in cui lo sarebbero su un sistema di
produzione tramite connessione TCP all'indirizzo 127.0.0.1.

