# Ambienti applicativi

Per ambienti applicativi intendiamo l'insieme di sistemi informatici utilizzati per il ciclo di vita del software.

## Sviluppo

Ambiente entro il quale viene sviluppata l'applicazione. Normalmente corrisponde alle postazioni
utilizzate dagli sviluppatori quindi duplicato per ognuno di essi.

## Collaudo

## Produzione

## Manutenzione